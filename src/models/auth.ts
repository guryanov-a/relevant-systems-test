import { UserInfo } from './user';

export interface LoginResponse extends Response {
  data: LoginResponseData;
}

export interface LoginResponseData {
  profile: UserInfo;
  token: string;
}
