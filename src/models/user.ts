export interface UserCreds {
  phone: string;
  password: string;
}

export interface UserInfo {
  birthDate: string;
  email: string;
  emailConfirmed: boolean;
  firstName: string;
  id: string;
  lastName: string;
  middleName: string;
  phone: string;
  sex: string;
}
