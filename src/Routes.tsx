import React from 'react';
import { connect } from 'react-redux';
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch,
} from 'react-router-dom';
import ExamsPage from './pages/exams';
import ProfilePage from './pages/profile';
import LoginPage from './pages/login';
import LogoutPage from './pages/logout';

const LoggedInRoutes = [
  <Redirect key="loggedInRedirectFromLoginPage" from='/login' to='/' />,
  <Redirect key="loggedInRedirectFromRootPage" exact from='/' to='/exams' />,
  <Route key="loggedInExamsPage" path="/exams" component={ ExamsPage } />,
  <Route key="loggedInProfilePage" path="/profile" component={ ProfilePage } />,
  <Route key="loggedInLogoutPage" path="/logout" component={ LogoutPage } />
];

const LoggedOutRoutes = [
  <Redirect key="loggedOutRedirectFromLogoutPage" from='/logout' to='/' />,
  <Redirect key="loggedOutRedirectFromRootPage" exact from='/' to='/login' />,
  <Route key="loggedOutLoginPage" path="/login" component={ LoginPage } />,
];

interface Props {
  loggedIn: boolean;
  dispatch: Function;
}

const Routes = ({loggedIn}: Props) => (
  <Router>
    <Switch>
      {[
        !loggedIn && LoggedOutRoutes,
        loggedIn && LoggedInRoutes,
        <Route key="page404" render={() => (<>404</>)} />
      ]}
    </Switch>
  </Router>
);

export default connect((state: any) => ({
  loggedIn: state.auth.isAuthenticated,
}))(Routes);
