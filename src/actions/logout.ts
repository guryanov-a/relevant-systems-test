import { Action } from 'redux';

// request
export const LOGOUT_REQUEST = 'LOGOUT_REQUEST';

export const requestLogout = (): Action => ({
  type: LOGOUT_REQUEST,
});

// success
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';

export const receiveLogout = (): Action => ({
  type: LOGOUT_SUCCESS,
});
