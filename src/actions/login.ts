import { Action } from 'redux';
import { UserCreds, UserInfo } from '../models';

// request
export const LOGIN_REQUEST: string = 'LOGIN_REQUEST';

export interface RequestLoginAction extends Action {
  payload: {
    creds: UserCreds;
  };
}

export const requestLogin = (creds: UserCreds): RequestLoginAction => ({
  type: LOGIN_REQUEST,
  payload: {
    creds,
  },
});

// failure
export const LOGIN_FAILURE: string = 'LOGIN_FAILURE';

export interface LoginErrorAction extends Action {
  payload: {
    errorMessage: string;
  };
}

export const loginError = (errorMessage: string): LoginErrorAction => ({
  type: LOGIN_FAILURE,
  payload: {
    errorMessage,
  },
});

// success
export const LOGIN_SUCCESS: string = 'LOGIN_SUCCESS';

export interface ReceiveLoginAction extends Action {
  payload: {
    user: UserInfo;
  };
}

export const receiveLogin = (profile: UserInfo): ReceiveLoginAction => ({
  type: LOGIN_SUCCESS,
  payload: {
    user: profile,
  },
});
