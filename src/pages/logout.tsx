import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { requestLogout } from '../actions';

interface Props {
  dispatch: Function;
}

const LogoutPage = ({ dispatch }: Props) => {
  useEffect(() => {
    dispatch(requestLogout());
  }, []);

  return (<></>);
};

export default connect()(LogoutPage);
