import React from 'react';
import LoggedOutLayout from '../layouts/logged-out';
import LoginSection from '../components/LoginSection';

const LoginPage = () => (
  <LoggedOutLayout>
    <LoginSection />
  </LoggedOutLayout>
);

export default LoginPage;
