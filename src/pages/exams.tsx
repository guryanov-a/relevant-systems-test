import React from 'react';
import BaseLayout from '../layouts/base';
import ExamsList from '../components/ExamsList';

const ExamsPage = () => (
  <BaseLayout title="Экзамены">
    <ExamsList />
  </BaseLayout>
);

export default ExamsPage;
