import React from 'react';
import BaseLayout from '../layouts/base';
import ProfileInfo from '../components/ProfileInfo';

const ProfilePage = () => (
  <BaseLayout title="Профиль">
    <ProfileInfo />
  </BaseLayout>
);

export default ProfilePage;
