import { AnyAction } from 'redux';
import {
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  LOGOUT_SUCCESS
} from '../actions';

interface AuthState {
  isFetching: boolean;
  isAuthenticated: boolean;
  errorMessage?: string;
}

const defaultState: AuthState = {
  isFetching: false,
  isAuthenticated: !!localStorage.getItem('access_token'),
};

export default function auth(state: AuthState = defaultState, {type, payload}: AnyAction) {
  switch (type) {
    case LOGIN_REQUEST:
      return {
        isFetching: true,
        isAuthenticated: false,
      };
    case LOGIN_SUCCESS:
      return {
        isFetching: false,
        isAuthenticated: true,
      };
    case LOGIN_FAILURE:
      return {
        isFetching: false,
        isAuthenticated: false,
        errorMessage: payload.errorMessage,
      };
    case LOGOUT_SUCCESS:
      return {
        isFetching: false,
        isAuthenticated: false,
      };
    default:
      return state;
  }
}
