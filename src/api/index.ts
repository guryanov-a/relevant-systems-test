import axios from 'axios';
import { UserCreds } from '../models';

const apiInstance = axios.create({
  baseURL: 'https://api-applicant.spkfr.ru/v1/',
});
const accessToken = localStorage.getItem('access_token');
if (accessToken) {
  apiInstance.defaults.headers.common['Authorization'] = `Bearer ${accessToken}`;
}

export function login(creds: UserCreds) {
  return apiInstance.post('/account/login', creds);
}

export function getExams() {
  return apiInstance.get('/exams');
}

export function getAuthtorizedUserInfo() {
  return apiInstance.get('/account/profile');
}

export default apiInstance;
