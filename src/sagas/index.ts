import { all } from 'redux-saga/effects';
import { watchLoginUserAsync } from './login';
import { watchLogoutUserAsync } from './logout';

export default function* rootSaga() {
  yield all([
    watchLoginUserAsync(),
    watchLogoutUserAsync(),
  ]);
}
