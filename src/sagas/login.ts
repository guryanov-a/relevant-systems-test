import {
  put,
  call,
  takeLatest,
} from 'redux-saga/effects';
import {
  loginError,
  receiveLogin,
  LOGIN_REQUEST,
  RequestLoginAction,
} from '../actions';
import { LoginResponse } from '../models';
import apiInstance, { login } from '../api';

function* loginUserAsync(action: RequestLoginAction) {
    const { payload: { creds } } = action;

    try {
      const response: LoginResponse = yield call(login, creds);
      const { token, profile } = response.data;
      localStorage.setItem('access_token', token);
      apiInstance.defaults.headers.common['Authorization'] = `Bearer ${token}`;
      yield put(receiveLogin(profile));
    } catch (error) {
      yield put(loginError(error.response.data.message));
    }
}

export function* watchLoginUserAsync() {
  yield takeLatest(LOGIN_REQUEST, loginUserAsync);
}
