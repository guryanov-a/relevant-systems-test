import { takeLeading, put } from 'redux-saga/effects';
import { LOGOUT_REQUEST, receiveLogout } from '../actions';
import apiInstance from '../api';

function* logoutUser() {
  localStorage.removeItem('access_token');
  delete apiInstance.defaults.headers.common['Authorization'];
  yield put(receiveLogout());
}

export function* watchLogoutUserAsync() {
  yield takeLeading(LOGOUT_REQUEST, logoutUser);
}
