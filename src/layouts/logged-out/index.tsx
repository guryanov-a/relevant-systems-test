import React, { ReactNode } from 'react';
import HeaderUnauthorized from '../../components/HeaderUnauthorized';

interface Props {
  children?: ReactNode
}

const LoggedOutLayout = (props: Props) => (
  <>
    <HeaderUnauthorized />
    <div>{ props.children }</div>
  </>
);

export default LoggedOutLayout;
