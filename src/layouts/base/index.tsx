import React, { ReactNode } from 'react';
import styles from './styles.module.scss';
import Logo from '../../components/Logo';
import Nav from '../../components/Nav';

interface Props {
  children?: ReactNode;
  title: string;
}

export default ({ children, title }: Props) => (
  <div className={styles.page}>
    <aside className={styles.sidebar}>
      <div className="container-fluid">
        <Logo />
        <hr />
        <Nav />
      </div>
    </aside>
    <main className={styles.main}>
      <header className={styles.main__header}>
        <div className="container-fluid">
          <h1>{title}</h1>
        </div>
      </header>
      <div className={styles.main__content}>
        <div className="container-fluid">
          { children }
        </div>
      </div>
    </main>
  </div>
);
