import React from 'react';
import Logo from '../Logo';
import styles from './styles.module.scss';

const HeaderUnauthorized = () => (
  <header className={styles.header}>
    <Logo />
  </header>
);

export default HeaderUnauthorized;
