import React from 'react';
import { Link, NavLink } from 'react-router-dom';
import styles from './styles.module.scss';
import ProfilePreview from '../../components/ProfilePreview';

const Nav = () => (
  <nav>
    <ProfilePreview />
    <hr />
    <ul className="nav flex-column">
      <li className="nav-item">
        <NavLink className={`nav-link ${styles.nav__link}`} to="/exams">Exams</NavLink>
      </li>
    </ul>
    <Link to="/logout">Logout</Link>
  </nav>
);

export default Nav;
