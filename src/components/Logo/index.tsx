import React from 'react';
import { Link } from 'react-router-dom';
import logo from './logo.svg';

const LoggedOutLayout = () => (
  <Link to="/">
    <img
      width="50"
      height="50"
      src={logo}
      alt="Logo"
    />
  </Link>
);

export default LoggedOutLayout;
