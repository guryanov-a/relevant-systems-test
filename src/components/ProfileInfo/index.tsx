import React, { useState, useEffect } from 'react';
import { getAuthtorizedUserInfo } from '../../api';

const ProfileInfo = () => {
  const [profileInfo, setProfileInfo]: any = useState({});

  useEffect(() => {
    getAuthtorizedUserInfo()
      .then((response: any) => {
        setProfileInfo(response.data);
      });
  }, []);

  return (
    <dl>
      <dt>birthDate:</dt>
      <dd>{new Date(profileInfo.birthDate).toLocaleDateString()}</dd>

      <dt>email:</dt>
      <dd>{profileInfo.email}</dd>

      <dt>emailConfirmed:</dt>
      <dd>{profileInfo.emailConfirmed}</dd>

      <dt>firstName:</dt>
      <dd>{profileInfo.firstName}</dd>

      <dt>lastName:</dt>
      <dd>{profileInfo.lastName}</dd>

      <dt>middleName:</dt>
      <dd>{profileInfo.middleName}</dd>

      <dt>phone:</dt>
      <dd>{profileInfo.phone}</dd>

      <dt>sex:</dt>
      <dd>{profileInfo.sex}</dd>
    </dl>
  );
};

export default ProfileInfo;
