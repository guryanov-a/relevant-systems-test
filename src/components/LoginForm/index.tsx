import React, { FormEvent, useState } from 'react';
import { connect } from 'react-redux';
import styles from './styles.module.scss';
import { requestLogin } from '../../actions';

interface Props {
  dispatch: Function;
  errorMessage: string;
}

const LoginForm = ({ dispatch, errorMessage }: Props) => {
  const [phone, setPhone] = useState('');
  const [password, setPassword] = useState('');

  function handleSubmit(e: FormEvent<HTMLFormElement>): void {
    e.preventDefault();
    dispatch(requestLogin({phone, password}));
  }

  function handlePhoneChange(e: FormEvent<HTMLInputElement>): void {
    const target = e.target as HTMLInputElement;
    setPhone(target.value);
  }

  function handlePasswordChange(e: FormEvent<HTMLInputElement>): void {
    const target = e.target as HTMLInputElement;
    setPassword(target.value);
  }

  return  (
    <form className={styles.form} onSubmit={handleSubmit}>
      <h1>Вход</h1>
      <div className="form-group">
        <label htmlFor="phone-number">Мобильный телефон</label>
        <div className={styles.phoneField}>
          <input
            type="tel"
            className={`form-control ${styles.phoneField__control}`}
            id="phone-number"
            value={phone}
            onChange={handlePhoneChange}
            required
          />
        </div>
      </div>
      <div className="form-group">
        <label htmlFor="password">Password</label>
        <input
          type="password"
          className="form-control"
          id="password"
          value={password}
          onChange={handlePasswordChange}
          required
        />
      </div>
      {
        errorMessage &&
        (
          <div
            className="alert alert-danger"
            role="alert"
          >
            {errorMessage}
          </div>
        )
      }
      <button
        type="submit"
        className="btn btn-primary btn-block"
      >
        Вход
      </button>
    </form>
  );
};

export default connect((state: any) => ({
  errorMessage: state.auth.errorMessage,
}))(LoginForm);
