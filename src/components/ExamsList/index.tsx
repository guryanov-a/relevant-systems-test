import React, { useEffect, useState } from 'react';
import { Table } from 'react-bootstrap';
import { getExams } from '../../api';

export interface ExamInfo {
  address: string;
  city: string;
  date: string;
  duration: number;
  examinationCenterName: string;
  id: string;
  key: string;
  occupationalStandardTitle: string;
  qualificationLevel: string;
  qualificationTitle: string;
  status: string;
  statusChangeTime: string;
}

const ExamsList = () => {
  const [exams, setExams] = useState([]);

  useEffect(() => {
    getExams()
      .then((response) => {
        setExams(response.data.exams);
      });
  }, []);

  const Exams = exams.map((exam: ExamInfo) => (
    <tr key={exam.id}>
      <td>{new Date(exam.date).toLocaleDateString()}</td>
      <td>
        <div>{exam.qualificationTitle}</div>
        <div>{exam.address}</div>
      </td>
      <td>{exam.qualificationLevel}</td>
      <td>{exam.city}</td>
      <td>{exam.status}</td>
    </tr>
  ));

  return (
    <Table striped bordered hover>
      <thead>
        <tr>
          <th>Дата и Время</th>
          <th>Квалификация</th>
          <th>Уровень</th>
          <th>Город</th>
          <th>Статус</th>
        </tr>
      </thead>
      <tbody>
        {Exams}
      </tbody>
    </Table>
  );
};

export default ExamsList;
