import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { getAuthtorizedUserInfo } from '../../api';

const ProfilePreview = () => {
  const [fullName, setFullName] = useState('');

  useEffect(() => {
    getAuthtorizedUserInfo()
      .then((response) => {
        const { firstName, lastName } = response.data;
        setFullName(`${firstName} ${lastName}`);
      })
  }, []);

  return (
    <Link to="/profile">{fullName}</Link>
  );
};

export default ProfilePreview;
