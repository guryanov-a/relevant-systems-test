import React from 'react';
import styles from './styles.module.scss';
import LoginForm from '../LoginForm';

const LoginSection = () => (
  <section className={styles.section}>
    <LoginForm />
  </section>
);

export default LoginSection;
