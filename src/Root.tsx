import React from 'react';
import { Provider } from 'react-redux';
import 'bootstrap/dist/css/bootstrap.min.css';
import configureStore from './store';
import Routes from './Routes';
import './Root.scss';

const store = configureStore();

const Root = () => (
  <Provider store={ store }>
    <Routes />
  </Provider>
);

export default Root;
